package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {

		byte someByte = 5; 
		short someShort = 10;
		int someInt = 1000 ;
		long someLong = 10_000_000L;
		float someFloat = 10.55f;
		double someDouble = 100000.44;
		boolean someBool = false;
		char someChar = 'a';
		
		System.out.println("someByte: " + someByte);
		System.out.println("someShort: " + someShort);
		System.out.println("someInt: " + someInt);
		System.out.println("someLong: " + someLong);
		System.out.println("someFloat: " + someFloat);
		System.out.println("someDouble: " + someDouble);
		System.out.println("someBool: " + someBool);
		System.out.println("someChar: " + someChar);
		
		System.out.println("-------------------------");
		
		someByte = Byte.MAX_VALUE; 
		someShort = Short.MAX_VALUE;
		someInt = Integer.MAX_VALUE;
		someLong = Long.MAX_VALUE;
		someFloat = Float.MAX_VALUE;
		someDouble = Double.MAX_VALUE;
		someBool = true;
		someChar = Character.MAX_VALUE;

		System.out.println("max byte: " + someByte);
		System.out.println("max short: " + someShort);
		System.out.println("max int: " + someInt);
		System.out.println("max long: " + someLong);
		System.out.println("max float: " + someFloat);
		System.out.println("max double: " + someDouble);
		System.out.println("max bool: " + someBool);
		System.out.println("max char: " + someChar);
		
		
		System.out.println("-------------------------");
		int overflowingNumber =  Integer.MAX_VALUE;
		System.out.println(overflowingNumber);
		overflowingNumber++; 
		System.out.println(overflowingNumber); 

		System.out.println("-------------------------");
		float bigNumber = 10000f;
		System.out.println(bigNumber);
		bigNumber = bigNumber + 0.00001f; 
		System.out.println(bigNumber);

		double bigDouble = 10000;
		System.out.println(bigNumber); 
		bigDouble = bigDouble + 0.00001;
		System.out.println(bigDouble); 
		
		
		}
}
